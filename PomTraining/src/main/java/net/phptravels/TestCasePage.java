package net.phptravels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utils.PropertyFileReader;
import utils.TestApp;

public class TestCasePage {

    WebDriver driver = TestApp.getInstance().getDriver();
    PropertyFileReader prop = new PropertyFileReader();
    String testCaseHeadingElement = prop.getProperty("TestCasePage","test.case.heading.element");

    public TestCasePage verifyTestCasePage(String expectedString){
        Assert.assertEquals(driver.findElement(By.xpath(testCaseHeadingElement)).getText(),expectedString);
        return this;
    }
}

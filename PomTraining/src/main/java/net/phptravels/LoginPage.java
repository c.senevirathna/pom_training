package net.phptravels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utils.PropertyFileReader;
import utils.TestApp;

public class LoginPage {
    WebDriver driver = TestApp.getInstance().getDriver();
    PropertyFileReader prop = new PropertyFileReader();
    String userNameElement = prop.getProperty("LoginPage","user.name.element");
    String passwordElement = prop.getProperty("LoginPage","password.element");
    String loginButtonElement = prop.getProperty("LoginPage","login.button.element");
    String loginSuccessElement = prop.getProperty("LoginPage","login.success.element");
    String loginUnsuccessElement = prop.getProperty("LoginPage","login.unsuccess.element");

    public HomePage loginSuccess(String username, String password){
        loginAs(username, password);
        Assert.assertEquals(driver.findElement(By.xpath(loginSuccessElement)).getText(),"chacha");
        return new HomePage();
    }

    public LoginPage loginUnsuccessful(String username, String password){
        loginAs(username,password);
        Assert.assertEquals(driver.findElement(By.xpath(loginUnsuccessElement)).getText(),"Your email or password is incorrect!");
        return this;
    }
    public void loginAs(String username, String password){
        TestApp.getInstance().waitForElement(By.name(userNameElement),30);
        driver.findElement(By.name(userNameElement)).sendKeys(username);
        driver.findElement(By.name(passwordElement)).sendKeys(password);
        driver.findElement(By.xpath(loginButtonElement)).click();
    }
}

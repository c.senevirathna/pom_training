package net.phptravels;

import static org.testng.Assert.assertEquals;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.PropertyFileReader;
import utils.TestApp;

public class ProductsPage {

    WebDriver driver= TestApp.getInstance().getDriver();
    PropertyFileReader prop= new PropertyFileReader();
    String searchTextboxElement = prop.getProperty("ProductsPage","search.textbox.element");
    String searchButtonElement = prop.getProperty("ProductsPage","search.button.element");
    String searchedResultElement = prop.getProperty("ProductsPage","searched.element");
    String addToCartElement = prop.getProperty("ProductsPage","addtocart.element");
    String viewCartElement = prop.getProperty("ProductsPage","view.cart.element");

    public ViewCartPage viewCartForSearchedProduct(String searchValue){
        TestApp.getInstance().waitForElement(By.id(searchTextboxElement),30);
        driver.findElement(By.id(searchTextboxElement)).sendKeys(searchValue);
        driver.findElement(By.className(searchButtonElement)).click();
        assertEquals(driver.findElement(By.xpath(searchedResultElement)).getText(),"SEARCHED PRODUCTS");
        driver.findElement(By.className(addToCartElement)).click();
        TestApp.getInstance().waitForElement(By.linkText(viewCartElement),30);
        driver.findElement(By.linkText(viewCartElement)).click();
        return  new ViewCartPage();
    }

    public HomePage addToCart(){
        TestApp.getInstance().waitForElement(By.id(searchTextboxElement),30);
        driver.findElement(By.className(addToCartElement)).click();
        return  new HomePage();
    }
}

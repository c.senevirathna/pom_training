package net.phptravels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import utils.PropertyFileReader;
import utils.TestApp;

import static utils.PropertyFileReader.verifyFileDownload;

public class ViewCartPage {
    WebDriver driver= TestApp.getInstance().getDriver();
    PropertyFileReader prop= new PropertyFileReader();
    String checkoutElement = prop.getProperty("ViewCartPage","checkout.element");
    String addressElement = prop.getProperty("ViewCartPage","address.element");
    String messageElement = prop.getProperty("ViewCartPage","message.element");
    String placeOrderButtonelement = prop.getProperty("ViewCartPage","place.order.button.element");
    String nameOnCardElement = prop.getProperty("ViewCartPage","name.on.card.element");
    String cardNumberElement = prop.getProperty("ViewCartPage","card.number.element");
    String cvcElement = prop.getProperty("ViewCartPage", "cvc.element");
    String expMonthElement = prop.getProperty("ViewCartPage","exp.month.element");
    String expYearElement = prop.getProperty("ViewCartPage","exp.year.element");
    String confirmOrderElement = prop.getProperty("ViewCartPage","confirm.order.element");
    String orderPlacedElement = prop.getProperty("ViewCartPage","order.placed.element");
    String orderConfirmMessageElement = prop.getProperty("ViewCartPage","order.confirm.message.element");
    String downloadInvoiceElement = prop.getProperty("ViewCartPage","download.invoice.element");
    String continueElement = prop.getProperty("ViewCartPage","continue.element");
    String homePageElement = prop.getProperty("ViewCartPage","home.page.element");
    public HomePage placeOrder(String message,String nameOnCard,String cardNumber, String csv,String expMonth,String expYear){
        driver.findElement(By.className(checkoutElement)).click();
        Assert.assertEquals(driver.findElement(By.xpath(addressElement)).getText(),"Address Details");
        driver.findElement(By.name(messageElement)).sendKeys(message);
        driver.findElement(By.xpath(placeOrderButtonelement)).click();
        TestApp.getInstance().waitForElement(By.name(nameOnCardElement),30);
        driver.findElement(By.name(nameOnCardElement)).sendKeys(nameOnCard);
        driver.findElement(By.name(cardNumberElement)).sendKeys(cardNumber);
        driver.findElement(By.name(cvcElement)).sendKeys(csv);
        driver.findElement(By.name(expMonthElement)).sendKeys(expMonth);
        driver.findElement(By.name(expYearElement)).sendKeys(expYear);
        driver.findElement(By.id(confirmOrderElement)).click();
        Assert.assertEquals(driver.findElement(By.xpath(orderPlacedElement)).getText(),"ORDER PLACED!");
        Assert.assertEquals(driver.findElement(By.xpath(orderConfirmMessageElement)).getText() ,"Congratulations! Your order has been confirmed!");
        //verifyDownloadedFile(downloadInvoiceElement);
        driver.findElement(By.linkText(continueElement)).click();
        Assert.assertEquals(driver.findElement(By.xpath(homePageElement)).getText(),"FEATURES ITEMS");
        return new HomePage();
    }

    public void verifyDownloadedFile(String downloadElement){
        String downloadDir = System.getProperty("C:\\Users\\User") + "/downloads";
        ChromeOptions options = new ChromeOptions();
        options.addArguments("download.default_directory=" + downloadDir);
        driver.findElement(By.linkText(downloadElement)).click();
        verifyFileDownload(downloadDir, "invoice.txt");
    }
}

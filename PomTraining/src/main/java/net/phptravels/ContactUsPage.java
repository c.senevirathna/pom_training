package net.phptravels;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utils.PropertyFileReader;
import utils.TestApp;

public class ContactUsPage{

    WebDriver driver = TestApp.getInstance().getDriver();
    PropertyFileReader prop = new PropertyFileReader();
    String getInTouchElement = prop.getProperty("ContactUsPage","get.in.touch.element");
    String nameElement = prop.getProperty("ContactUsPage","name.element");
    String emailElement = prop.getProperty("ContactUsPage","email.element");
    String subjectElement = prop.getProperty("ContactUsPage","subject.element");
    String messageElement = prop.getProperty("ContactUsPage","message.element");
    String chooseFileElement = prop.getProperty("ContactUsPage","choose.file.element");
    String submitButtonELement = prop.getProperty("ContactUsPage","submit.btn.element");
    Alert alert;
    String successAlertElement = prop.getProperty("ContactUsPage","success.alert.element");
    String homeBtnElement = prop.getProperty("ContactUsPage","home.btn.element");
    String exerciseTextElement = prop.getProperty("ContactUsPage","exercise.text.element");

    public ContactUsPage verifyContactUsPage(){
        Assert.assertEquals(driver.findElement(By.xpath(getInTouchElement)).getText(),"GET IN TOUCH");
        return this;
    }

    public HomePage addAnInquiry(String name, String email,String subject,String message,String filePath,String successAlert,String adText){
        driver.findElement(By.name(nameElement)).sendKeys(name);
        driver.findElement(By.name(emailElement)).sendKeys(email);
        driver.findElement(By.name(subjectElement)).sendKeys(subject);
        driver.findElement(By.name(messageElement)).sendKeys(message);
        TestApp.getInstance().waitForElement(By.name(chooseFileElement),20);
        driver.findElement(By.name(chooseFileElement)).sendKeys(filePath);
        driver.findElement(By.name(submitButtonELement)).click();
        alert = TestApp.getInstance().waitForAlert(30);
        alert.accept();
        TestApp.getInstance().waitForElement(By.xpath(successAlertElement),20);
        Assert.assertEquals(driver.findElement(By.xpath(successAlertElement)).getText(),successAlert);
        driver.findElement(By.linkText(homeBtnElement)).click();
        Assert.assertEquals(driver.findElement(By.xpath(exerciseTextElement)).getText(),adText);
        return new HomePage();
    }

}

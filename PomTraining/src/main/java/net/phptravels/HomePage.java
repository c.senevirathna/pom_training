package net.phptravels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.PropertyFileReader;
import utils.TestApp;

public class HomePage {
    WebDriver driver = TestApp.getInstance().getDriver();
    PropertyFileReader prop = new PropertyFileReader();
    String signInButtonElement = prop.getProperty("HomePage","signIn.page.element");
    String productPageElement = prop.getProperty("HomePage","products.page.element");
    String shoppingCartElement = prop.getProperty("HomePage","view.cart.page.element");
    String contactUsElement = prop.getProperty("HomePage","contact.us.element");
    String testCaseElement = prop.getProperty("HomePage","test.case.element");

    public LoginPage navigateToSignInPage(){
        driver.findElement(By.xpath(signInButtonElement)).click();
        return new LoginPage();
    }

    public ProductsPage navigateToProductPage(){
        driver.findElement(By.xpath(productPageElement)).click();
        return new ProductsPage();
    }

    public ViewCartPage navigateToCart(){
        driver.findElement(By.linkText(shoppingCartElement)).click();
        return new ViewCartPage();
    }

    public ContactUsPage navigateToContactUs(){
        driver.findElement(By.linkText(contactUsElement)).click();
        return new ContactUsPage();
    }

    public TestCasePage navigateToTestCasesPage(){
        driver.findElement(By.linkText(testCaseElement)).click();
        return new TestCasePage();
    }
}
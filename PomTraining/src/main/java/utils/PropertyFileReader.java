package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyFileReader {

    private Properties getData(String fileName){
        ClassLoader classLoader = getClass().getClassLoader();
        File file =new File(classLoader.getResource(fileName+".properties").getFile());
        FileInputStream fileInput = null;
        try {
            fileInput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties prop = new Properties();

        try {
            prop.load(fileInput);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }

    public static void verifyFileDownload(String downloadDir, String fileName) {
        File dir = new File(downloadDir);
        File[] dirContents = dir.listFiles();

        // Check if the directory contains the downloaded file
        for (File file : dirContents) {
            if (file.getName().equals(fileName)) {
                System.out.println("File downloaded successfully: " + fileName);
                return;
            }
        }
        System.out.println("File not downloaded: " + fileName);
    }

    public String getProperty(String fileName,String key){
        return getData(fileName).getProperty(key);

    }
}

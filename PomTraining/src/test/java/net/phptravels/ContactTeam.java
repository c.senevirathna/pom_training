package net.phptravels;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.TestApp;

public class ContactTeam{

    private final String name = "QA Tester";
    private final String email = "firstname.secondname@domain.com";
    private final String subject = "How To Test";
    private final String message = "Testing is the process of executing a " +
            "program or part of a program with the intention of finding errors. The different phases of a " +
            "test life cycle are Test Planning and Control, Test Analysis and Design, Test Implementation " +
            "and Execution, Evaluating Exit Criteria and Reporting, and Test Closure.";
    private final String filePath = "E:\\Automation\\POM\\pom_training\\PomTraining\\src\\test\\resources\\WIN_20230603_20_04_02_Pro.jpg";
    private final String successAlert = "Success! Your details have been submitted successfully.";
    private final String adText = "AutomationExercise";
    HomePage createdCx2, homePage;

    @BeforeClass
    public void setUp() {
        TestApp.getInstance().openBrowser();
        TestApp.getInstance().navigateToURL();
        homePage = new HomePage();
        createdCx2 = homePage.navigateToSignInPage().loginSuccess("smc.chathurya@gmail.com","1qaz2WSX");
    }
    @Test(priority = 2)
    public void contactTeam() {
        createdCx2.navigateToContactUs()
                .verifyContactUsPage()
                .addAnInquiry(name,email,subject,message,filePath,successAlert,adText);
    }

    @AfterClass
    public void closeBrowser() {
        TestApp.getInstance().closeBrowser();
    }
}

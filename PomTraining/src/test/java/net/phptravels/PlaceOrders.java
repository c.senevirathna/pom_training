package net.phptravels;

import org.testng.annotations.*;
import utils.TestApp;

public class PlaceOrders {

    //WebDriver driver = TestApp.getInstance().getDriver();
    HomePage homePage;
    ViewCartPage viewCartPage;
    HomePage createdCx;
    LoginPage invalidCx;

    @BeforeClass
    public void setUp() {
        TestApp.getInstance().openBrowser();
        TestApp.getInstance().navigateToURL();
        homePage = new HomePage();

    }

    @Test(priority = 0)
    public void loginUnsuccessfulTC3() {
        invalidCx = homePage.navigateToSignInPage().loginUnsuccessful("smc.chathurya@gmail1234.com","1qaz2WSX");
    }

    @Test(priority = 1)
    public void loginSuccessfulTC2() {
        createdCx = homePage.navigateToSignInPage().loginSuccess("smc.chathurya@gmail.com","1qaz2WSX");
    }

    @Test(priority = 1)
    public void placeOrderBySearchedProductTest() {
        viewCartPage = createdCx.navigateToProductPage()
                .viewCartForSearchedProduct("top");
        viewCartPage.placeOrder("place order automation","Chathu","778324387","562","02","2026");
    }

    @Test(priority = 2)
    public void placeOrderForSelectedProductsTest() {
        viewCartPage = createdCx
                .navigateToProductPage()
                .addToCart()
                .navigateToCart();
        viewCartPage.placeOrder("order placed-Automation","Chathu122","778324387u","502","07","2029");
    }
    @AfterClass
    public void closeBrowser() {
        TestApp.getInstance().closeBrowser();
    }


}

package net.phptravels;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.TestApp;

public class ViewTestCases {

    HomePage homePage,loggedInUser;

    @BeforeMethod
    public void setUp() {
        TestApp.getInstance().openBrowser();
        TestApp.getInstance().navigateToURL();
        homePage = new HomePage();
        loggedInUser = homePage.navigateToSignInPage().loginSuccess("smc.chathurya@gmail.com","1qaz2WSX");
    }

    @Test(priority = 2)
    public void viewTestCases() {
        loggedInUser.navigateToTestCasesPage().verifyTestCasePage("TEST CASES");
    }
}
